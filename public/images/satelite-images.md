# Free Satelite Images

### Maxar free low quality
[Search for Imagery](https://discover.digitalglobe.com/)

### SentilHub
[EO Browser](https://apps.sentinel-hub.com/eo-browser/)

examples:
[1](https://sentinelshare.page.link/gPQ3)
[2](https://sentinelshare.page.link/tbKr)
[3](https://sentinelshare.page.link/w2MP)

### nasa worldview
[worldview.earthdata.nasa.gova](https://worldview.earthdata.nasa.gova)

examples:
[1](https://worldview.earthdata.nasa.gov/?l=MODIS_Combined_Thermal_Anomalies_All,Reference_Labels_15m(hidden),Reference_Features_15m(hidden),Coastlines_15m,VIIRS_NOAA20_CorrectedReflectance_TrueColor(hidden),VIIRS_SNPP_CorrectedReflectance_TrueColor(hidden),MODIS_Aqua_CorrectedReflectance_TrueColor(hidden),MODIS_Terra_CorrectedReflectance_TrueColor&lg=true&t=2022-03-23-T18%3A45%3A04Z)

### eumetsat
[view.eumetsat.int](https://view.eumetsat.int/productviewer)

---

### other sources
[gisgeography](https://gisgeography.com/free-satellite-imagery-data-list/)
